yaml = require('js-yaml');
fs   = require('fs');
sprintf = require('sprintf-js').sprintf;
jsrender = require('jsrender');
const nodeHtmlToImage = require('node-html-to-image')

aircrafts =
[
  'AV8BNA', // AV-8B
  // 'F-16C',
];

function buildChecklist(aircraft, sections, section, checklist)
{
  console.log('checklist ' + checklist.label);
  console.log('building checklist ' + checklist.label + ' for section ' + section.label + ' for aircraft ' + aircraft);

  var lineLength=53;
  const stringPadding='.';
  checklist.items.forEach(element => {
    var padding=lineLength - element.label.length  - element.action.length - 2;

    element.renderLine=element.label + ' ' + stringPadding.repeat(padding) + ' <b>' + element.action + '</b>';
  });

  const image = fs.readFileSync('./img/wildtextures-seamless-paper-texture-200.jpg');
  const base64Image = new Buffer(image).toString('base64');

  const imageCockpit = fs.readFileSync('./img/'+aircraft+'/cockpit-600.png');
  const base64Cockpit = new Buffer(imageCockpit ).toString('base64');

  var tmpl = jsrender.templates('./templates/checklist.html'); // Compile template
  var html = tmpl.render({sections: sections, section: section, checklist: checklist, backgroundImage: base64Image, cockpitImage: base64Cockpit});


  var path = './var/'+aircraft;
  var filename = path + '/' + checklist.code + '.png';

  if(!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }

  nodeHtmlToImage({
    output: filename,
    html: html,
  })
  .then(() => console.log(filename + ' written'));

  fs.writeFileSync(filename+'.html', html);
}

function buildSection(aircraft, sections, section, num)
{
  console.log('building section ' + section.label + ' for aircraft ' + aircraft);
  section.checklists.forEach(checklist => buildChecklist(aircraft, sections, section, checklist));
}

function buildAircraft(aircraft)
{
  var doc = yaml.safeLoad(fs.readFileSync('src/' + aircraft + '.yml', 'utf8'));
  console.log('building sections for ' + aircraft);

  doc.sections.forEach(section => buildSection(aircraft, doc.sections, section));
}

// Get document, or throw exception on error
try {
  console.log('========================================');
  console.log('building checklists');
  aircrafts.forEach(aircraft => buildAircraft(aircraft));

} catch (e) {
  console.log(e);
}
