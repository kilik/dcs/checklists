yaml = require('js-yaml');
fs   = require('fs');
sprintf = require('sprintf-js').sprintf;
jsrender = require('jsrender');
const nodeHtmlToImage = require('node-html-to-image')

aircrafts =
[
  'AV8BNA', // AV-8B
  // 'F-16C',
];

function buildRadio(conf, aircraft)
{
  //console.log(radio);
  console.log('building radio kneeboard for aircraft ' + aircraft);

  let radio1=aircraft.radios[0];
  let radio2=aircraft.radios[1];

  let channels1;
  let channels2;

  if(radio1.default) {
    channels1=conf.radios.default[radio1.default];
  }
  else {
    channels1=radio1.channels;
  }

  if(radio2.default) {
    channels2=conf.radios.default[radio2.default];
  }
  else {
    channels2=radio2.channels;
  }

  const image = fs.readFileSync('./img/wildtextures-seamless-paper-texture-200.jpg');
  const base64Image = new Buffer(image).toString('base64');

  console.log(channels1);
  console.log(channels2);

  let tmpl = jsrender.templates('./templates/radio.html'); // Compile template
  let html = tmpl.render({radio1: radio1, radio2: radio2, backgroundImage: base64Image, channels1: channels1, channels2: channels2});

  let path = './var/'+aircraft.code;
  let filename = path + '/' + '00-radio' + '.png';

  if(!fs.existsSync(path)) {
    fs.mkdirSync(path);
  }

  nodeHtmlToImage({
    output: filename,
    html: html,
  })
      .then(() => console.log(filename + ' written'));

  fs.writeFileSync(filename+'.html', html);

}

function buildAircraft(conf, aircraft)
{
  console.log('building radio kneeboard for ' + aircraft);

  console.log(aircraft);

  buildRadio(conf, aircraft)
  //aircraft.radios.forEach(radio => buildRadio(conf, aircraft, radio));
}

function buildAllRadio() {
  var conf = yaml.safeLoad(fs.readFileSync('src/radio.yml', 'utf8'));
  console.log('buildAllRadio');

  //console.log(conf);
  //console.log(conf.aircrafts);

  conf.conf.aircrafts.forEach(aircraft => buildAircraft(conf.conf, aircraft));
}

// Get document, or throw exception on error
try {
  console.log('========================================');
  buildAllRadio();
} catch (e) {
  console.log(e);
}
